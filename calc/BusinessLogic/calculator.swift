//
//  calculator.swift
//  calc
//
//  Created by alex on 16.01.2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation
import CoreData
import UIKit
public class Calculator{
    
    private lazy var updatedAtDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()
    
    let dateFormat = "yyyy-MM-dd"
    
    private let creditData:CreditData?
    private let coreDataManager:CoreDataManager
    private var parametriKredita = ParametriKredita()
    
    
    
    //var procentnay_stavka_v_month:Double = 0
    
    /*
     
     var full_kredit:Double = 0
     var pereplata:Double = 0
     var platesh_po_procentam:Double = 0
     var current_kredit:Double = 0
     //var nextDateForPayment:DateComponents = DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2018, month: 09, day: 03)
     var osnovnoy_dolg:Double = 0
     var interval_for_procents:Int = 0
     var procent_stavka_v_den:Double = 0
     */
    
    
    public init(creditData:CreditData,coreDataManager:CoreDataManager) {
        self.creditData = creditData
        self.coreDataManager = coreDataManager
    }
    
    
    func getDaysInYearFromDate(date:Date) -> Double?{
        print("HERE")
        let interval = Calendar.current.dateInterval(of: .year, for: date)!
        print(interval)
        // Compute difference in days:
        let daysInYear = Calendar.current.dateComponents([.day], from: interval.start, to: interval.end).day!
        print("NUMBER IN YEAR")
        print("34")
        print(daysInYear)
        return Double(daysInYear)
        
    }
    
    
    func changeDayInDate(date:Date,day:Int) -> Date?{
        
        var paymentDatePoGraphicy = date
        var components = Calendar.current.dateComponents([.day,.month,.year,.hour,.minute,.nanosecond], from: paymentDatePoGraphicy)
        components.day = day
        paymentDatePoGraphicy = Calendar.current.date(from: components)!
        
        return paymentDatePoGraphicy
        
    }
    
    func getFirstDateOfNextYear(date:Date) -> Date?{
        
        let paymentDatePoGraphicy = date
        
        
        
        var firstDayNextYear = Calendar.current.date(byAdding: .year, value: 1, to: paymentDatePoGraphicy)
        
        
        var components = Calendar.current.dateComponents([.day,.month,.year,.hour,.minute,.nanosecond], from: firstDayNextYear!)
        components.day = 1
        components.month = 1
        
        
        
            firstDayNextYear = Calendar.current.date(from: components)!
        print("firstDayNextYear")
        print(firstDayNextYear!)
        return firstDayNextYear
        
    }
    
    
    
    func checkFirstPlatesh(creditData:CreditData) -> CreditData{
         print("HERE")
        var localCreditData = creditData
        
        //check if data pervogo platesha menshe po graphicy
        
        let date_Vidachi_kredita = Calendar.current.date(from: creditData.dataVidachiKredita)
        print(date_Vidachi_kredita!)
        
        var paymentDatePoGraphicy =  changeDayInDate(date: date_Vidachi_kredita!, day: creditData.dayPlatesha)
        
        print(paymentDatePoGraphicy!)
        var components = Calendar.current.dateComponents([.day,.month,.year,.hour,.minute,.nanosecond], from: paymentDatePoGraphicy!)
        components.day = creditData.dayPlatesha
        
        print("DAY NUMBER \(String(describing: components.day))")
      
        paymentDatePoGraphicy = Calendar.current.date(from: components)
        print(paymentDatePoGraphicy!)
        
   
        
        localCreditData.credit = creditData.credit
        localCreditData.dataPlatesha = Calendar.current.dateComponents([.month,.day,.year],from:paymentDatePoGraphicy!)
        if(date_Vidachi_kredita! != paymentDatePoGraphicy!){
            print("data nachala kredita ranche dati platesha po graphicy")
            let diffInDays = Calendar.current.dateComponents([.day], from: date_Vidachi_kredita!, to: paymentDatePoGraphicy!).day
            print(diffInDays!)
            
            
            let interval = Calendar.current.dateInterval(of: .year, for: date_Vidachi_kredita!)!
            print(interval)
            // Compute difference in days:
            let days = Calendar.current.dateComponents([.day], from: interval.start, to: interval.end).day!
            print("NUMBER IN YEAR")
            print(days)
            
            print("DIFF IN DAYS")
            print(diffInDays!)
            
            print( Double(diffInDays!) / Double(days))
            
            
            var firstPlatesh = creditData.credit * Double(diffInDays!) / Double(days) * creditData.procent / 100
            print("HERE")
            
            firstPlatesh = firstPlatesh.rounded(toPlaces: 2)
            print(firstPlatesh)
            
         
            
            addOneCreditStringtoCoreDataContext(dateOplati: paymentDatePoGraphicy!, osnovnoyDolg: 0, procent: firstPlatesh, summ_for_pay: firstPlatesh, ostatok_kredita: creditData.credit)
            
            
            print("NEW CREDIT PARAMETERS")
            
            
            
            
            
            var  nextMonth = paymentDatePoGraphicy
            print("BEFORE NEXT MONTH: \(String(describing: nextMonth))")
            nextMonth = Calendar.current.date(byAdding: .month, value: 1, to: nextMonth!)
       
            // creditDataNew.credit = ostatok_kredita
            localCreditData.dataPlatesha = Calendar.current.dateComponents([.month,.day,.year],from:nextMonth!)
            //////////
            
            let newRangeForPay = Calendar.current.dateComponents([.month], from: localCreditData.dataVidachiKredita, to: localCreditData.dataPlatesha!)
            print("00! \(newRangeForPay)")
            
            let newRangeForPayInt = newRangeForPay.month
            print("!!!!!!")
            print(newRangeForPayInt!)
            
            localCreditData.range = Double(Int(localCreditData.range) - newRangeForPayInt!)
            
            //разобраться с этим все зависит от первого платежа, если он не совпадает
            localCreditData.range+=1
   
            print(localCreditData.credit)
            print("NOVIY S \(localCreditData.dataPlatesha!)")
          
        }
        calculateParametriKredita(creditData:localCreditData,newDosrochniy_platesh: 0)
        
        calculateKredit(creditData: localCreditData)
        return localCreditData
        
    }
    
    
    
    func calculateParametriKredita(creditData:CreditData,newDosrochniy_platesh:Double){
        
        print("calculateParametriKredita")
        print("creditData.credit \(creditData.credit)")
        print("dataVidachiKredita \(creditData.dataVidachiKredita)")
     
       
        
        
        
        
        parametriKredita.procentnay_stavka_v_month = creditData.procent / 12 / 100
        print("Procent stavka v month \(parametriKredita.procentnay_stavka_v_month)")
        
        let i1 = (1+parametriKredita.procentnay_stavka_v_month) ^^ creditData.range
        parametriKredita.k = (parametriKredita.procentnay_stavka_v_month * i1) / (i1 - 1)
        
        
        if(newDosrochniy_platesh != 0){
            parametriKredita.plata_v_month = newDosrochniy_platesh
        } else{
             parametriKredita.plata_v_month = parametriKredita.k * creditData.credit
        }
        
        
        
        
       
        
        
        print("Annuitelniy platesh v month \(Double(round(100*parametriKredita.plata_v_month)/100))")
        
       
        
        
        //ushest last i perviy month
        parametriKredita.full_kredit = Double(round(100*parametriKredita.plata_v_month*creditData.range)/100)
        print("range: \(creditData.range)")
        print("FULL KREDIT: \(parametriKredita.full_kredit)")
        parametriKredita.pereplata = parametriKredita.full_kredit - creditData.credit
        print("Pereplata: \(parametriKredita.pereplata)")
        
        
        parametriKredita.platesh_po_procentam =  creditData.credit * parametriKredita.procentnay_stavka_v_month
        print("PROCENT PO PLATESHY: \(parametriKredita.platesh_po_procentam)")
        
         print("parametriKredita.procentnay_stavka_v_month: \(parametriKredita.procentnay_stavka_v_month)")
        
        parametriKredita.procent_stavka_v_den = creditData.procent/365/100
    }
    
    
    
    func getDateFromDateComponents(dateComponents:DateComponents) -> Date{
        let test = DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: dateComponents.year, month: dateComponents.month, day: dateComponents.day)
        
        
        let myDate1 = Calendar.current.date(from: test)
        print(myDate1!)
        
       
        return myDate1!
    }
    
    func calculateKredit(creditData:CreditData){
        
        var ostatok_osnovnogo_dolga:Double = 0
        var osnovnoy_dolg:Double = 0
        var oplata_procentov:Double = 0
       
        
        var paymentDatePoGraphicy = getDateFromDateComponents(dateComponents: creditData.dataPlatesha!)
        
        
        print("PAYMENT PO GR\(paymentDatePoGraphicy)")
        oplata_procentov = parametriKredita.platesh_po_procentam
        ostatok_osnovnogo_dolga = creditData.credit
         var ostatok_kredita_final_day:Double = 0
        print("PARAMETRI KREDITA")
        print(ostatok_osnovnogo_dolga)
        print(oplata_procentov)
      //  var ostatok_kredita_final_day:Double = 0
        
        for i in 0..<Int(creditData.range){
            
            
            let previous_payment_date = Calendar.current.date(byAdding: .month, value: -1, to: paymentDatePoGraphicy)
            
            
            
            
            print("!!!paymentDatePoGraphicy \(i) \(paymentDatePoGraphicy)")
            print("!!!previose_date \(i) \(previous_payment_date!)")
            //oplata_procentov = ostatok_osnovnogo_dolga *
            
            let previous_payment_date_Components = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: previous_payment_date!)
            let paymentDatePoGraphicy_Components = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: paymentDatePoGraphicy)
            
            
            let previous_payment_date_Components1 = Calendar.current.date(from: previous_payment_date_Components)
            let paymentDatePoGraphicy_Components1 = Calendar.current.date(from: paymentDatePoGraphicy_Components)
            
            let rangebetweenDays = Calendar.current.dateComponents([.day], from: previous_payment_date_Components1!, to: paymentDatePoGraphicy_Components1!)
            
            
            
            print("rangebetweenDays.day \(rangebetweenDays.day!)")
            
            
            
            if(paymentDatePoGraphicy_Components.year == previous_payment_date_Components.year){
                
                print("GOD SOVPADAET")
                
                let rangeDays = Calendar.current.dateComponents([.day], from: previous_payment_date_Components1!, to: paymentDatePoGraphicy_Components1!).day
                
                let interval = Calendar.current.dateInterval(of: .year, for: previous_payment_date_Components1!)!
                print(interval)
                // Compute difference in days:
                let daysInYear = Calendar.current.dateComponents([.day], from: interval.start, to: interval.end).day!
                print("NUMBER IN YEAR")
                print(daysInYear)
                
                print("------------")
                print(parametriKredita.procentnay_stavka_v_month)
                print(rangeDays!)
                oplata_procentov = ostatok_osnovnogo_dolga * creditData.procent * Double(rangeDays!)
                
                oplata_procentov = oplata_procentov / Double((100*daysInYear))
                 print("***")
                print(oplata_procentov)
              oplata_procentov = oplata_procentov.rounded(toPlaces: 2)
                
                
                print("PROCENTS-->   \(paymentDatePoGraphicy)  \(oplata_procentov)")
               // oplata_procentov = oplata_procentov.rounded(toPlaces: 3)
                
                print(1300000*12.5*31/365)
                
                if(oplata_procentov == 12386.725054644807){
                    print("HERE")
                }
                
                var scale: Int16 = 3
                let scale2: Int16 = 2
                var behavior = NSDecimalNumberHandler(roundingMode: .plain, scale: scale2, raiseOnExactness: false, raiseOnOverflow: false, raiseOnUnderflow: false, raiseOnDivideByZero: true)
                print(parametriKredita.plata_v_month)
                print("PLATA PROCENT \(oplata_procentov)")
                
                
                
                parametriKredita.plata_v_month = Double(truncating: NSDecimalNumber(value: parametriKredita.plata_v_month).rounding(accordingToBehavior: behavior))
                print(parametriKredita.plata_v_month)
                 print("oplata_procentov \(oplata_procentov)")
                oplata_procentov = oplata_procentov.rounded(toPlaces: 2)
                   print("oplata_procentov \(oplata_procentov)")
                //oplata_procentov = ostatok_kredita * parametriKredita.procentnay_stavka_v_month
                osnovnoy_dolg = parametriKredita.plata_v_month - oplata_procentov
          //      print(29247.31969304045 - 12386.725054)
            //     print(29247.32 - 12386.73)
                osnovnoy_dolg = Double(truncating: NSDecimalNumber(value: osnovnoy_dolg).rounding(accordingToBehavior: behavior))
                scale = 2
                behavior = NSDecimalNumberHandler(roundingMode: .plain, scale: scale, raiseOnExactness: false, raiseOnOverflow: false, raiseOnUnderflow: false, raiseOnDivideByZero: true)
               // osnovnoy_dolg = Double(NSDecimalNumber(value: osnovnoy_dolg).rounding(accordingToBehavior: behavior))
                osnovnoy_dolg = osnovnoy_dolg.rounded(toPlaces: 2)
                print("OSN DOLG:  \(osnovnoy_dolg)")
            //  osnovnoy_dolg =  osnovnoy_dolg.rounded(toPlaces: 3)
                
                   print(osnovnoy_dolg)
                print(17486.545.rounded(toPlaces: 2))
                print(osnovnoy_dolg)
                print(0.6849.rounded(toPlaces: 2))
                 print(osnovnoy_dolg)
                
             
              //  let roundedValue1 = NSDecimalNumber(value: 0.6844).rounding(accordingToBehavior: behavior)
               // let roundedValue2 = NSDecimalNumber(value: 17486.545).rounding(accordingToBehavior: behavior)

                //print(roundedValue1) // returns 0.684
                //print(roundedValue2) // returns 0.685
                
                
            }
            else {
                print("NE SOVPADAET")
                
                
                let firstDayNextYear = getFirstDateOfNextYear(date: previous_payment_date_Components1!)
                print("FIRST DAY NEXT YEAR")
                print(firstDayNextYear!)
                
                let rangeDaysDO_NG = Calendar.current.dateComponents([.day], from: previous_payment_date_Components1!, to: firstDayNextYear!).day! - 1
                
                let rangeDaysAFTER_NG = Calendar.current.dateComponents([.day], from: firstDayNextYear!, to: paymentDatePoGraphicy_Components1!).day! + 1
                
                
                
                
                let intervalDO = Calendar.current.dateInterval(of: .year, for: previous_payment_date_Components1!)!
                print(intervalDO)
                // Compute difference in days:
                let daysInYear_PREVIOUSE = Calendar.current.dateComponents([.day], from: intervalDO.start, to: intervalDO.end).day!
                print("NUMBER IN YEAR")
                print(daysInYear_PREVIOUSE)
                
                
                
                
                let intervalPOSLE = Calendar.current.dateInterval(of: .year, for: paymentDatePoGraphicy_Components1!)!
                print(intervalPOSLE)
                // Compute difference in days:
                let daysInYear_AFTER = Calendar.current.dateComponents([.day], from: intervalPOSLE.start, to: intervalPOSLE.end).day!
                   print("daysInYear_PREVIOUSE \(daysInYear_PREVIOUSE)")
                  print("daysInYear_AFTER \(daysInYear_AFTER)")
                
                
                   print("rangeDaysDO_NG \(rangeDaysDO_NG)")
                 print("rangeDaysAFTER_NG \(rangeDaysDO_NG)")
                print("------------")
                print(parametriKredita.procentnay_stavka_v_month)
               // print(rangeDays)
               
                
                ostatok_osnovnogo_dolga = ostatok_osnovnogo_dolga.rounded(toPlaces: 2)
                
                print("ostatok_osnovnogo_dolga \(ostatok_osnovnogo_dolga)")
                print("creditData.procent \(creditData.procent)")
                var oplata_procentov_DO_NG = ostatok_osnovnogo_dolga * creditData.procent * Double(rangeDaysDO_NG)
                oplata_procentov_DO_NG = oplata_procentov_DO_NG.rounded(toPlaces: 2)
                print("oplata_procentov_DO_NG \(oplata_procentov_DO_NG) \(ostatok_osnovnogo_dolga) \(creditData.procent) \(rangeDaysDO_NG)")
                
                
                oplata_procentov_DO_NG = oplata_procentov_DO_NG / Double((100*daysInYear_PREVIOUSE))
              oplata_procentov_DO_NG = oplata_procentov_DO_NG.rounded(toPlaces: 2)
                   print("daysInYear_PREVIOUSE \(100*daysInYear_PREVIOUSE)")
                   print("oplata_procentov_DO_NG \(oplata_procentov_DO_NG)")
                
                var oplata_procentov_AFTER = ostatok_osnovnogo_dolga * creditData.procent * Double(rangeDaysAFTER_NG)
                oplata_procentov_AFTER = oplata_procentov_AFTER.rounded(toPlaces: 2)
                 print("oplata_procentov_AFTER \(oplata_procentov_AFTER) \(ostatok_osnovnogo_dolga) \(creditData.procent) \(rangeDaysAFTER_NG)")
                
                
                oplata_procentov_AFTER = oplata_procentov_AFTER / Double((100*daysInYear_AFTER))
                oplata_procentov_AFTER = oplata_procentov_AFTER.rounded(toPlaces: 2)
                print("daysInYear_AFTER \(100*daysInYear_AFTER)")
                print("oplata_procentov_AFTER \(oplata_procentov_AFTER)")
                
                
                
                oplata_procentov = oplata_procentov_DO_NG + oplata_procentov_AFTER
                
                
                print("FULL SUMM PROCENTS   \(oplata_procentov)")
                
                
                print(1300000*12.5*31/365)
                
                
                //oplata_procentov = ostatok_kredita * parametriKredita.procentnay_stavka_v_month
                osnovnoy_dolg = parametriKredita.plata_v_month - oplata_procentov
                print(osnovnoy_dolg)
                 osnovnoy_dolg =  osnovnoy_dolg.rounded(toPlaces: 2)
                  print(osnovnoy_dolg)
                
                
            }
            
           
            if(i == Int(creditData.range - 1)) {
                ostatok_kredita_final_day = ostatok_osnovnogo_dolga.rounded(toPlaces: 2)
              
                print("HERE OSTATOK \(ostatok_kredita_final_day)")
            }
            
            ostatok_osnovnogo_dolga -= osnovnoy_dolg
            print("ostatok_osnovnogo_dolga \(ostatok_osnovnogo_dolga)")
            
            
            print("PAYMENT DATE PO GRAPHICY \(paymentDatePoGraphicy)")
            print("PAYMENT osnovnoy_dolg \(osnovnoy_dolg)")
           oplata_procentov = oplata_procentov.rounded(toPlaces: 2)
            print("PAYMENT procnet \(oplata_procentov)")
            ostatok_osnovnogo_dolga = ostatok_osnovnogo_dolga.rounded(toPlaces: 2)
            
            print("summ_for_pay: \(parametriKredita.plata_v_month)")
            print("ostatok_kredita: \(ostatok_osnovnogo_dolga)")
            
            
            if(oplata_procentov == 11760.774831233653){
                print("HERE")
            }
            
            
            
            addOneCreditStringtoCoreDataContext(dateOplati: paymentDatePoGraphicy, osnovnoyDolg: osnovnoy_dolg, procent: oplata_procentov, summ_for_pay: parametriKredita.plata_v_month, ostatok_kredita: ostatok_osnovnogo_dolga)
            
            
            paymentDatePoGraphicy = Calendar.current.date(byAdding: .month, value: 1, to: paymentDatePoGraphicy)!
            
        }
        
        
        
        
       
        
        
        coreDataManager.deleteLastStringandAddNewOneForLastMonth()
        //последний процент считается так, уможноам ставку в день  0.00032054794520547944 на количество дней с 3 августа по 22 августа то етсь 19 и умножаем на 5811.72 остаток кредита -  в основной долг заносим всю сумму и получаем сумму к оплате
       var t = Calendar.current.date(from:creditData.dataVidachiKredita)
        let lastDatePoDogovory = Calendar.current.date(byAdding: .month, value: Int(creditData.range), to: t!)
       // Calendar.current.date(from:lastDatePoDogovory)
        
        //var lastDatePoDogovory = Calendar.current.date(from: lastDatePoDogovory!)
        paymentDatePoGraphicy = Calendar.current.date(byAdding: .month, value: -1, to: paymentDatePoGraphicy)!
        var rangeForProcentPay = Calendar.current.dateComponents([.day], from: lastDatePoDogovory!, to: paymentDatePoGraphicy)
        ostatok_osnovnogo_dolga = 0
        var rangeInt = rangeForProcentPay.day
        rangeInt  = rangeInt! - 2
        print(" Interval: \(rangeInt)")
        
        print("parametriKredita.procent_stavka_v_den \(parametriKredita.procent_stavka_v_den)")
        print("range \(Double(rangeInt!))")
        print("ostatok \(ostatok_osnovnogo_dolga)")
        
        
        
        
        let interval = Calendar.current.dateInterval(of: .year, for: lastDatePoDogovory!)!
        print(interval)
        // Compute difference in days:
        let daysInYear = Calendar.current.dateComponents([.day], from: interval.start, to: interval.end).day!
        print("NUMBER IN YEAR")
        print(daysInYear)
        
        
        
        
        
        
        
        let procent_stavka_v_den = creditData.procent/Double(daysInYear)/100
        
        let lastProcent =   Double(ostatok_kredita_final_day * procent_stavka_v_den * Double(rangeInt!)).rounded(toPlaces: 2)
        print("last PROCENT")
        print(lastProcent)
        
        print(7976.47 * 14 * procent_stavka_v_den)
        
        
        
      
        addOneCreditStringtoCoreDataContext(dateOplati: lastDatePoDogovory!, osnovnoyDolg: ostatok_kredita_final_day, procent: lastProcent, summ_for_pay: ostatok_kredita_final_day+lastProcent, ostatok_kredita: ostatok_osnovnogo_dolga)
        paymentDatePoGraphicy = Calendar.current.date(byAdding: .month, value: 1, to: paymentDatePoGraphicy)!
        
       
    }
    
    
  
  
   
    






func checkDate(date_dosrochniy_platesh:Date,date_po_graphicy:Date)-> Bool{
    
    let day_date_dosrochniy_platesh = Calendar.current.component(.day,from:date_dosrochniy_platesh)
    let day_date_po_graphicy = Calendar.current.component(.day,from:date_po_graphicy)
    if(day_date_dosrochniy_platesh == day_date_po_graphicy){
        return true
    } else {
        return false
    }
    
    
}
    
    
    func pay_dosrochniy_v_den_po_graphicy(creditData:CreditData,amount:Double, at date:Date){
        print("THIS PLATESH SOVPADAET")
        print(amount)
        //добавить проверку если досрочный платеж совпадает с датой официального платежа, значит не считаем разницу со следующей датой!
        let predicate = NSPredicate(format: "payment_date == %@", date as NSDate)
        let request: NSFetchRequest<SberEntity> = SberEntity.fetchRequest()
        let sort = NSSortDescriptor(key: #keyPath(SberEntity.payment_date), ascending: true)
        request.sortDescriptors = [sort]
        
        request.predicate = predicate
        
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: request,
                                                              managedObjectContext: coreDataManager.mainManagedObjectContext,
                                                              sectionNameKeyPath: nil,
                                                              cacheName: nil)
        print("check if was string at this date")
        do {
            print("found string at this date already")
            try fetchedResultsController.performFetch()
            
            
        } catch {
            print("Unable to Perform Fetch Request")
            print("\(error), \(error.localizedDescription)")
        }
        
     
        
        if let creditStringatThisDate = fetchedResultsController.fetchedObjects?.last {
            print("FOUNDDDDD")
        
            
            print(creditStringatThisDate.payment_date!)
        print("ostatok_po_dolgy \(creditStringatThisDate.ostatok_po_dolgy)")
            
            let localOsnovnoyDolg = creditStringatThisDate.osnovnoy_dolg + amount
             let localProcent = creditStringatThisDate.procent
            let localAmount = creditStringatThisDate.summ_for_pay + amount
            let localostatok_kredita = creditStringatThisDate.ostatok_po_dolgy - localOsnovnoyDolg
            print("localostatok_kredita!!!   \(localostatok_kredita)")
            
            
            
            
            fetchedResultsController.managedObjectContext.delete(creditStringatThisDate)
            
            
            do{
                try fetchedResultsController.managedObjectContext.save()
            } catch{
                fatalError()
            }
            
            
            print("Add string po graphicy  \(date)")
            addOneCreditStringtoCoreDataContext(dateOplati: date, osnovnoyDolg: localOsnovnoyDolg, procent: localProcent, summ_for_pay: localAmount, ostatok_kredita: localostatok_kredita)
            
            
            //pereschet vsego credita
            
            var creditDataNew = creditData
            print("NEW CREDIT PARAMETERS")
            
            
            creditDataNew.credit = localostatok_kredita
            
            
          
          
            let nextMonth = Calendar.current.date(byAdding: .month, value: 1, to: date)
            print("NEXT MONTH: \(nextMonth!)")
            // creditDataNew.credit = ostatok_kredita
           
            //////////
            creditDataNew.dataVidachiKredita = Calendar.current.dateComponents([.month,.day,.year],from:nextMonth!)
            creditDataNew.dataEndOfKredit = creditData.dataEndOfKredit
             creditDataNew.dataPlatesha =  creditDataNew.dataVidachiKredita
            print("sdf")
            print("creditDataNew.dataVidachiKredita \(creditDataNew.dataVidachiKredita)")
              print("creditDataNew.dataLastKredita \(creditDataNew.dataEndOfKredit!)")
            let newRangeForPay = Calendar.current.dateComponents([.month], from: creditDataNew.dataVidachiKredita, to: creditDataNew.dataEndOfKredit!)
            print("00! \(newRangeForPay)")
            
            let newRangeForPayInt = newRangeForPay.month
            print("!!!!!!")
            print(newRangeForPayInt!)
            
            creditDataNew.range = Double(newRangeForPayInt!)
            
            //разобраться с этим все зависит от первого платежа, если он не совпадает
            creditDataNew.range+=2
            
            ////////////////
           // creditDataNew.credit = localostatok_kredita
            
            print(creditDataNew.credit)
            print("NOVIY S GRAPHIC \(creditDataNew.dataPlatesha!)")
            print("reditDataNew.range \(creditDataNew.range)")
            print("NOVIY CREDIT \(creditDataNew.dataPlatesha!)")
            print("NOVIY CREDIT ostatok \(creditDataNew.credit)")
            print("NOVIY CREDIT data \(creditDataNew.dataPlatesha!)")
            
            print("CLEAR TABLE FROM DATE \(date)")
            
            //DELETE LAST STRING
           
            
            coreDataManager.clearTableFrom(date: date)
             coreDataManager.deleteLastStringandAddNewOneForLastMonth()
            calculateParametriKredita(creditData:creditDataNew, newDosrochniy_platesh: 0)
            
            
            
            
            calculateKredit(creditData: creditDataNew)
            
            
            
            
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
    }
    
    func getCoreDataForDate( date:Date) -> SberEntity{
        
        
        
        
        
        
        
        let fetchRequest: NSFetchRequest<SberEntity> = SberEntity.fetchRequest()
      
      fetchRequest.predicate = NSPredicate(format: "payment_date == %@", date as NSDate)
        let sort = NSSortDescriptor(key: #keyPath(SberEntity.payment_date), ascending: true)
        fetchRequest.sortDescriptors = [sort]
        
    
        var fetchedResultsController: NSFetchedResultsController<SberEntity>!
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                              managedObjectContext: coreDataManager.mainManagedObjectContext,
                                                              sectionNameKeyPath: nil,
                                                              cacheName: nil)
      
        let results: [SberEntity]?
        do {
         try fetchedResultsController.performFetch()
        } catch {
            print("Unable to Perform Fetch Request")
            print("\(error), \(error.localizedDescription)")
        }
     
        if let result = fetchedResultsController?.fetchedObjects?.last {
            return result
        }
        
        
        return SberEntity()
        
    }
    
    
    
    
    func dosrhochnoye_pogashenie(creditData:CreditData,amount:Double, at date:Date){
        
        var plata_v_month_do_dosrochnogo_platesha:Double = 0
        var dolg_do_dosrochnogo:Double = 0
        var dolg_posle_dosrochnogo:Double = 0
        print("PLATA V MONTH DO DOSROCHNOGO PLATESHA \(parametriKredita.plata_v_month)")
      plata_v_month_do_dosrochnogo_platesha = parametriKredita.plata_v_month.rounded(toPlaces: 2)
        
         dolg_do_dosrochnogo = creditData.credit
        let dataPlateshaPoGraphicy = Calendar.current.date(from: creditData.dataVidachiKredita)
       print(dataPlateshaPoGraphicy!)
        let paymentDatePoGraphicy =  changeDayInDate(date: dataPlateshaPoGraphicy!, day: creditData.dayPlatesha)
          print(paymentDatePoGraphicy!)
        let sovpadaet_dosrhochniy_s_graphicom = checkDate(date_dosrochniy_platesh: date,date_po_graphicy: paymentDatePoGraphicy!)
        print("SOVPADAET LI")
        print(sovpadaet_dosrhochniy_s_graphicom)
        
        if(!sovpadaet_dosrhochniy_s_graphicom){
            
            var ostatok_kredita:Double = 0
            var osnovnoy_dolg:Double = 0
            var oplata_procentov:Double = 0
            var interval_for_procents:Int = 0
            print("AMOUNT")
            print(amount)
            if(amount == 1300.0){
                print("HERE")
            }
            var procents_for_previouse_period:Double = 0
            
            
            
            let request: NSFetchRequest<SberEntity> = SberEntity.fetchRequest()
            let sort = NSSortDescriptor(key: #keyPath(SberEntity.payment_date), ascending: true)
            request.sortDescriptors = [sort]
            
            
            var predicate = NSPredicate(format: "payment_date < %@", date as NSDate)
            request.predicate = predicate
            
            var fetchedResultsController: NSFetchedResultsController<SberEntity>!
            fetchedResultsController = NSFetchedResultsController(fetchRequest: request,
                                                                  managedObjectContext: coreDataManager.mainManagedObjectContext,
                                                                  sectionNameKeyPath: nil,
                                                                  cacheName: nil)
            print("TRY TO FETCH")
            do {
                print("DOSRHOCHNO Data fetched")
                try fetchedResultsController.performFetch()
                
                
            } catch {
                print("Unable to Perform Fetch Request")
                print("\(error), \(error.localizedDescription)")
            }
            print("COUNT")
            print(fetchedResultsController.fetchedObjects?.count as Any)
            
            //добавить проверку если дата такая есть то добавляем досрочный платеж уже к существующей строке!
            if let previouseCreditString = fetchedResultsController?.fetchedObjects?.last {
               
                print("OSTATOK DO DOSROCHNOGO: \(previouseCreditString.ostatok_po_dolgy)")
                dolg_do_dosrochnogo = previouseCreditString.ostatok_po_dolgy
                
                print("HERE")
                print(previouseCreditString.payment_date!)
                 print(previouseCreditString.procent)
                print(previouseCreditString.osnovnoy_dolg)
                   print(previouseCreditString.ostatok_po_dolgy)
               
                
                
                
                
                
                
                interval_for_procents = Calendar.current.dateComponents([.day], from: previouseCreditString.payment_date!, to:date).day!
                
                print("INTERVAL: \(interval_for_procents)")
                print("parametriKredita.procent_stavka_v_den")
                print(parametriKredita.procent_stavka_v_den)
               
                var daysInYear = getDaysInYearFromDate(date: date)!
                let procent_stavka_v_den = creditData.procent/daysInYear/100
                print("&&&&&&")
                print(procent_stavka_v_den)
                print(previouseCreditString.ostatok_po_dolgy)
                procents_for_previouse_period =   Double(previouseCreditString.ostatok_po_dolgy * procent_stavka_v_den * Double(interval_for_procents)).rounded(toPlaces: 2)
                if (previouseCreditString.procent < 0) {
                    procents_for_previouse_period += (-1) * previouseCreditString.procent
                }
               print(287000 * 1 * 0.00034246575342465754)
                
            //    if(procents_for_previouse_period < )
                
                
                print("procents_for_previouse_period: \(procents_for_previouse_period)")
                
                osnovnoy_dolg = Double(amount - procents_for_previouse_period).rounded(toPlaces: 2)
                ostatok_kredita = Double(previouseCreditString.ostatok_po_dolgy - osnovnoy_dolg).rounded(toPlaces: 2)
                
                if(procents_for_previouse_period >= amount){
                    procents_for_previouse_period =  amount - procents_for_previouse_period
                    osnovnoy_dolg = 0
                    ostatok_kredita = previouseCreditString.ostatok_po_dolgy 
                    
                } else{
                    osnovnoy_dolg = Double(amount - procents_for_previouse_period).rounded(toPlaces: 2)
                }
                
                
                if(previouseCreditString.osnovnoy_dolg == 0){
                    //osnovnoy_dolg = 0
                    // ostatok_kredita = previouseCreditString.ostatok_po_dolgy
                }
         
                oplata_procentov = procents_for_previouse_period
                
          
                
               
                
                
                
                
            } else {
                // если первый платеж
                interval_for_procents = 0
                procents_for_previouse_period = 0
                
                osnovnoy_dolg = Double(amount).rounded(toPlaces: 2)
                oplata_procentov = procents_for_previouse_period
                
                
                
                ostatok_kredita = Double(creditData.credit  - osnovnoy_dolg).rounded(toPlaces: 2)
            }
            
            
            
            
            //проверка есть ли в этот день запись уже
            
            //добавить проверку если досрочный платеж совпадает с датой официального платежа, значит не считаем разницу со следующей датой!
            predicate = NSPredicate(format: "payment_date == %@", date as NSDate)
            request.predicate = predicate
            
            
            fetchedResultsController = NSFetchedResultsController(fetchRequest: request,
                                                                  managedObjectContext: coreDataManager.mainManagedObjectContext,
                                                                  sectionNameKeyPath: nil,
                                                                  cacheName: nil)
            print("check if was string at this date")
            do {
                print("found string at this date already")
                try fetchedResultsController.performFetch()
                
                
            } catch {
                print("Unable to Perform Fetch Request")
                print("\(error), \(error.localizedDescription)")
            }
            
            var localAmount = amount
            var localOsnovnoyDolg = osnovnoy_dolg
            var localostatok_kredita = ostatok_kredita
            
            if let creditStringatThisDate = fetchedResultsController?.fetchedObjects?.last {
                print("FOUNDDDDD")
                print(localAmount)
                if(creditStringatThisDate.osnovnoy_dolg == 0){
                    print(localostatok_kredita)
                    localOsnovnoyDolg = amount
                    localostatok_kredita -= creditStringatThisDate.procent
                }
                
                
                print(creditStringatThisDate.payment_date!)
                localAmount += creditStringatThisDate.summ_for_pay
                
                
                
                
                fetchedResultsController.managedObjectContext.delete(creditStringatThisDate)
                
                
                do{
                    try fetchedResultsController.managedObjectContext.save()
                } catch{
                    fatalError()
                }
                
                
                
                
                
            }
            
            
            
            
            
            
            print("ADD ONE STRING  \(date) \(osnovnoy_dolg)")
            
            
            addOneCreditStringtoCoreDataContext(dateOplati: date, osnovnoyDolg: localOsnovnoyDolg, procent: oplata_procentov, summ_for_pay: localAmount, ostatok_kredita: localostatok_kredita)
            
            
            //очистить с даты следующего штатного платежа/
            
            print("CLEAR TABLE FROM DATE \(date)")
            coreDataManager.clearTableFrom(date: date)
            
            // Меняем количество денег которые надо оплатить по графику
            print("NEXT PAYMENT")
            
            
            
            //Dodelat pereshet novogo kredita - nt trogat stroky dosrhochnogo platesha 11/08/2019
 
            
            
            
            
            let requestNew: NSFetchRequest<SberEntity> = SberEntity.fetchRequest()
            requestNew.sortDescriptors = [sort]
            fetchedResultsController = NSFetchedResultsController(fetchRequest: requestNew,
                                                                  managedObjectContext: coreDataManager.mainManagedObjectContext,
                                                                  sectionNameKeyPath: nil,
                                                                  cacheName: nil)
            print("TRY TO FETCH")
            do {
                print("fetch")
                try fetchedResultsController.performFetch()
                
                
            } catch {
                print("Unable to Perform Fetch Request")
                print("\(error), \(error.localizedDescription)")
            }
            
            
            
            
            
            let sberEntityArray = fetchedResultsController.fetchedObjects
            // Меняем количество денег которые надо оплатить по графику
            
          
            print(sberEntityArray?.count as Any)
          
            
            
            //берем текущее значение и предыдущее, то есть наш досрочный платеж и ближайший по графику
            if let dosrhochniyPlatesh = sberEntityArray?[(sberEntityArray?.count)!-2],
                let nextPlateshPoGraphicy = sberEntityArray?[(sberEntityArray?.count)!-1]
            {
               // plata_v_month_do_dosrochnogo_platesha = nextPlateshPoGraphicy.summ_for_pay
                print("PREVIOUSE VALUE: \(dosrhochniyPlatesh.payment_date!) \(dosrhochniyPlatesh.osnovnoy_dolg)  \(dosrhochniyPlatesh.procent)  \(dosrhochniyPlatesh.summ_for_pay) \(dosrhochniyPlatesh.ostatok_po_dolgy)")
               
                 print("NEXT VALUE: \(nextPlateshPoGraphicy.payment_date!) \(nextPlateshPoGraphicy.osnovnoy_dolg)  \(nextPlateshPoGraphicy.procent)  \(nextPlateshPoGraphicy.summ_for_pay) \(nextPlateshPoGraphicy.ostatok_po_dolgy)")
            
              //  plata_v_month_do_dosrochnogo_platesha = nextPlateshPoGraphicy.summ_for_pay
                
                print("NEXT PLATESH PO GRAPHICY : \(plata_v_month_do_dosrochnogo_platesha)")
                
                if(nextPlateshPoGraphicy.osnovnoy_dolg == 0){
                   // dosrhochniyPlatesh.osnovnoy_dolg = 0
                  //  dosrhochniyPlatesh.procent = dosrhochniyPlatesh.summ_for_pay
                   
                } else{
                    
                }
                
                
                let rangeForProcentPay = Calendar.current.dateComponents([.day], from: dosrhochniyPlatesh.payment_date!, to: nextPlateshPoGraphicy.payment_date!)
                
                var rangeInt = rangeForProcentPay.day
                rangeInt  = rangeInt!
                print(" Interval: \(rangeInt!)")
                
                
                var daysInYear =  getDaysInYearFromDate(date: dosrhochniyPlatesh.payment_date!)!
                
                
                
                let procent_stavka_v_den = creditData.procent/daysInYear/100
                var ostatok:Double = 0
                
                if (rangeInt == 0){
                    ostatok = creditData.credit
                } else {
                    ostatok = dosrhochniyPlatesh.ostatok_po_dolgy
                }
                print(procent_stavka_v_den)
                print(" ostatok: \(ostatok)")
              
                var procents_for_previouse_period =   Double(ostatok * procent_stavka_v_den * Double(rangeInt!)).rounded(toPlaces: 2)
                 print(287.434.rounded(toPlaces: 2))
                print(287.435.rounded(toPlaces: 2))
                procents_for_previouse_period = procents_for_previouse_period.rounded(toPlaces: 2)
                print("testsafddssdffdsf")
                
                print(" procent_stavka_v_den: \(procent_stavka_v_den)")
                print(" procents_for_previouse_period: \(procents_for_previouse_period)")
                nextPlateshPoGraphicy.procent = procents_for_previouse_period
                
                
                //если мы платим столько что покрываем процент
                
                print("DOSROCHNIY PLATESH")
                print("dosrhochniyPlatesh.summ_for_pay \(dosrhochniyPlatesh.summ_for_pay)")
                print("nextPlateshPoGraphicy.summ_for_pay \(nextPlateshPoGraphicy.summ_for_pay)")
                print("DOSROCHNIY PLATESH")
                
                
                
                //DODELAT 26/06/2019 inogda neverno pokazivaet
                //OSTALSA ETOT KUSOK!!! 20.06.19
                //EDIT 08.08.2019
                if(dosrhochniyPlatesh.osnovnoy_dolg >= nextPlateshPoGraphicy.osnovnoy_dolg){
                    nextPlateshPoGraphicy.osnovnoy_dolg = 0
                    
                    if(dosrhochniyPlatesh.summ_for_pay < nextPlateshPoGraphicy.summ_for_pay){
                        nextPlateshPoGraphicy.summ_for_pay -= dosrhochniyPlatesh.summ_for_pay
                      //  nextPlateshPoGraphicy.procent = nextPlateshPoGraphicy.summ_for_pay
                               nextPlateshPoGraphicy.summ_for_pay = nextPlateshPoGraphicy.summ_for_pay.rounded(toPlaces: 2)
                         nextPlateshPoGraphicy.procent =  nextPlateshPoGraphicy.procent.rounded(toPlaces: 2)
                        if(nextPlateshPoGraphicy.procent > nextPlateshPoGraphicy.summ_for_pay){
                            nextPlateshPoGraphicy.summ_for_pay = nextPlateshPoGraphicy.procent
                        }
                        
                    } else {
                    
                    
                    
                   
                
                    nextPlateshPoGraphicy.summ_for_pay = nextPlateshPoGraphicy.procent
                    print("NEXT PLATESH PRO GRAPHICY")
                    nextPlateshPoGraphicy.summ_for_pay = nextPlateshPoGraphicy.summ_for_pay.rounded(toPlaces: 2)
                        
                    }
                    nextPlateshPoGraphicy.ostatok_po_dolgy = dosrhochniyPlatesh.ostatok_po_dolgy
                    print(nextPlateshPoGraphicy.summ_for_pay)
                    
                    
                    
                }
                
                if(dosrhochniyPlatesh.osnovnoy_dolg<nextPlateshPoGraphicy.osnovnoy_dolg){
                   print("OSNOVNOY DOLG PO GRAPHICY NE POKRIT DOSROCHNIM PLATESHEM")
                    nextPlateshPoGraphicy.osnovnoy_dolg -= dosrhochniyPlatesh.osnovnoy_dolg
                    
                    nextPlateshPoGraphicy.procent = -1
                  
                   nextPlateshPoGraphicy.summ_for_pay = -1
                }
                
                
                //nextPlateshPoGraphicy.osnovnoy_dolg = 0
                // nextPlateshPoGraphicy.summ_for_pay = nextPlateshPoGraphicy.procent
                
              
                
                
                var creditDataNew = creditData
                print("NEW CREDIT PARAMETERS")
                
                
                creditDataNew.credit = nextPlateshPoGraphicy.ostatok_po_dolgy
                
                
                var  nextMonth = nextPlateshPoGraphicy.payment_date
                print("BEFORE NEXT MONTH: \(nextMonth!)")
                nextMonth = Calendar.current.date(byAdding: .month, value: 1, to: nextMonth!)
                print("NEXT MONTH: \(nextMonth!)")
                // creditDataNew.credit = ostatok_kredita
                creditDataNew.dataPlatesha = Calendar.current.dateComponents([.month,.day,.year],from:nextMonth!)
                //////////
                
                let newRangeForPay = Calendar.current.dateComponents([.month], from: creditDataNew.dataVidachiKredita, to: creditDataNew.dataPlatesha!)
                print("00! \(newRangeForPay)")
                
                let newRangeForPayInt = newRangeForPay.month
                print("!!!!!!")
                print(newRangeForPayInt!)
                
                creditDataNew.range = Double(Int(creditDataNew.range) - newRangeForPayInt!)
                
                //разобраться с этим все зависит от первого платежа, если он не совпадает
                creditDataNew.range+=1
                
                ////////////////
                
                
                print(creditDataNew.credit)
                print("NOVIY S \(creditDataNew.dataPlatesha!)")
                
                print("NOVIY CREDIT \(creditDataNew.dataPlatesha!)")
                print("NOVIY CREDIT ostatok \(creditDataNew.credit)")
                print("NOVIY CREDIT data \(creditDataNew.dataPlatesha!)")
                
                
                //NEW 111
               
                  print("POSLE DOSROCHNOGO: \(ostatok)")
                
                dolg_posle_dosrochnogo = ostatok
                
                print("---------------")
                print(dolg_do_dosrochnogo)
                print(dolg_posle_dosrochnogo)
                print(plata_v_month_do_dosrochnogo_platesha)
                var newDosrochniy_platesh:Double  = 0
                print(6454.76 *   286904.32  / 279279.89)
                if(dolg_posle_dosrochnogo != dolg_do_dosrochnogo && dolg_do_dosrochnogo != 0) {
                
               newDosrochniy_platesh = plata_v_month_do_dosrochnogo_platesha * dolg_posle_dosrochnogo / dolg_do_dosrochnogo
                print("NEW DOSROCHNIY: \(newDosrochniy_platesh)")
                } else {
                    newDosrochniy_platesh = plata_v_month_do_dosrochnogo_platesha
                }
                calculateParametriKredita(creditData:creditDataNew,newDosrochniy_platesh:newDosrochniy_platesh)
                
                
                
                
                calculateKredit(creditData: creditDataNew)
                
            } else{
                print("HERE ALEX")
            }
            
          
        } else{
            
            pay_dosrochniy_v_den_po_graphicy(creditData: creditData,amount: amount,at: date)
        }
        
        
    }
    
    func getNextDateForPaymentFromThisDate(date:Date){
        
        
    }
    
    
    
    
    
    
    
    func addOneCreditStringtoCoreDataContext(dateOplati:Date,osnovnoyDolg:Double,procent:Double,summ_for_pay:Double, ostatok_kredita:Double) -> SberEntity{
        
        let entity:SberEntity
        var local_summ_for_pay:Double = summ_for_pay.rounded(toPlaces: 2)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        
        if NSEntityDescription.entity(forEntityName: "SberEntity", in: coreDataManager.mainManagedObjectContext) != nil{
            
            entity = SberEntity(context: coreDataManager.mainManagedObjectContext)
            entity.payment_date = dateOplati
            entity.osnovnoy_dolg = Double(round(100*osnovnoyDolg)/100)
            entity.procent = procent.rounded(toPlaces: 2)
            entity.summ_for_pay = local_summ_for_pay
            entity.ostatok_po_dolgy = Double(round(100*ostatok_kredita)/100)
            
            print("entity was added")
            
            return entity
            
        }
        return SberEntity()
    }
    
    
    
    
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
        
        
        
        
        
        
     //   let multiplier = pow(10, Double(places))
       // return Darwin.round(self * multiplier) / multiplier
     //   return Double((self * Double(places)).rounded(.toNearestOrEven)/Double(places))
 
}

precedencegroup PowerPrecedence { higherThan: MultiplicationPrecedence }
infix operator ^^ : PowerPrecedence
func ^^ (radix: Double, power: Double) -> Double {
    return Double(pow(Double(radix), Double(power)))
}
