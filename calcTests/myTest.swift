//
//  myTest.swift
//  calcTests
//
//  Created by Alex on 01.09.2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import XCTest

@testable import calc
class myTest: XCTestCase {
    
    
    
    var coreDataStack: TestCoreDataStack!
    var calculator: Calculator!
    var creditData:CreditData!
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        super.setUp()
        coreDataStack = TestCoreDataStack()
        creditData = CreditData()
        calculator = Calculator(creditData:creditData,coreDataManager:coreDataStack)
        
    }
    
    override func tearDown() {
        
        super.tearDown()
        coreDataStack = nil
        calculator = nil
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    
    func testAddValue() {
        
        // let calculator1 = calculator.checkFirstPlatesh(creditData: creditData)
        // print(calculator1)
        
        let testValue =     calculator.addOneCreditStringtoCoreDataContext(dateOplati: Calendar.current.date(from: DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2019, month: 07, day: 01    ))!, osnovnoyDolg: 0, procent: 0, summ_for_pay: 0, ostatok_kredita: 0)
        
        
        XCTAssertTrue(testValue.ostatok_po_dolgy == 0)
        XCTAssertTrue(testValue.procent == 0)
        XCTAssertTrue(testValue.osnovnoy_dolg == 0)
        XCTAssertTrue(testValue.payment_date == Calendar.current.date(from: DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2019, month: 07, day: 01    ))!)
    }
    
    
    
    func testContextIsSavedAfterAddString(){
        
        
        let testValue =     calculator.addOneCreditStringtoCoreDataContext(dateOplati: Calendar.current.date(from: DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2019, month: 07, day: 01    ))!, osnovnoyDolg: 10, procent: 0, summ_for_pay: 0, ostatok_kredita: 0)
        
        
        
        let result = calculator.getCoreDataForDate(date:  Calendar.current.date(from: DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2019, month: 07, day: 01    ))!)
        
        XCTAssertTrue(result.osnovnoy_dolg == 10)
        
        
        
    }
    
     func testCreditAtStart(){
         let localCreditData = calculator.checkFirstPlatesh(creditData:creditData)
        
        let result = calculator.getCoreDataForDate(date:  Calendar.current.date(from: DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2024, month: 07, day: 01    ))!)
        
        XCTAssertTrue(result.summ_for_pay == 29769.02)
    }
    
    
    
    
    func testAt2019_09_17_EQUALS_629042(){
    
    
    let localCreditData = calculator.checkFirstPlatesh(creditData:creditData)
    
    
    calculator.dosrhochnoye_pogashenie(creditData: localCreditData, amount: 1013000, at: Calendar.current.date(from: DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2019, month: 07, day: 01    ))!)
    calculator.dosrhochnoye_pogashenie(creditData: localCreditData, amount: 1000, at: Calendar.current.date(from: DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2019, month: 07, day: 15    ))!)
    calculator.dosrhochnoye_pogashenie(creditData: localCreditData, amount: 570, at: Calendar.current.date(from: DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2019, month: 07, day: 16    ))!)
    
    calculator.dosrhochnoye_pogashenie(creditData: localCreditData, amount: 9000, at: Calendar.current.date(from: DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2019, month: 07, day: 31    ))!)
    
    calculator.dosrhochnoye_pogashenie(creditData: localCreditData, amount: 1000, at: Calendar.current.date(from: DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2019, month: 08, day: 01    ))!)
    calculator.dosrhochnoye_pogashenie(creditData: localCreditData, amount: 1000, at: Calendar.current.date(from: DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2019, month: 08, day: 02    ))!)
    
    calculator.dosrhochnoye_pogashenie(creditData: localCreditData, amount: 1000, at: Calendar.current.date(from: DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2019, month: 08, day: 03    ))!)
    
    calculator.dosrhochnoye_pogashenie(creditData: localCreditData, amount: 680, at: Calendar.current.date(from: DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2019, month: 08, day: 08    ))!)
    
    
    calculator.dosrhochnoye_pogashenie(creditData: localCreditData, amount: 800, at: Calendar.current.date(from: DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2019, month: 08, day: 15    ))!)
    
    
       
        let result = calculator.getCoreDataForDate(date:  Calendar.current.date(from: DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2019, month: 09, day: 17    ))!)
        
        XCTAssertTrue(result.summ_for_pay == 6290.42)
        
        
        
        let result1 = calculator.getCoreDataForDate(date:  Calendar.current.date(from: DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2019, month: 08, day: 17    ))!)
        XCTAssertTrue(result1.summ_for_pay == 189.19)
        
        
        
          let result2 = calculator.getCoreDataForDate(date:  Calendar.current.date(from: DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2019, month: 09, day: 17    ))!)
     XCTAssertTrue(result2.summ_for_pay == 6290.42)
        // XCTAssertTrue(result2.summ_for_pay == 6289.15)
    }
    
}
