//
//  TestCoreDataStack.swift
//  calcTests
//
//  Created by Alex on 01.09.2019.
//  Copyright © 2019 Alex. All rights reserved.
//

//
//  TestCoreDataStack.swift
//  CampgroundManagerTests
//
//  Created by Alex on 01.09.2019.
//  Copyright © 2019 Razeware. All rights reserved.
//

import Foundation

import calc
import Foundation
import CoreData


class TestCoreDataStack: CoreDataManager {
    convenience init() {
        self.init(modelName: "sberModel")
    }
    override init(modelName: String) {
        super.init(modelName: modelName)
        let persistentStoreDescription =
            NSPersistentStoreDescription()
        persistentStoreDescription.type = NSInMemoryStoreType
        let container = NSPersistentContainer(name: modelName)
        container.persistentStoreDescriptions =
            [persistentStoreDescription]
        container.loadPersistentStores {
            (storeDescription, error) in
            if let error = error as NSError? {
                fatalError(
                    "Unresolved error \(error), \(error.userInfo)")
            }
        }
        self.persistentStoreCoordinator = container.persistentStoreCoordinator
      
    }
}
